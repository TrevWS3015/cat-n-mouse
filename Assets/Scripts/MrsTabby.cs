﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MrsTabby : MonoBehaviour {

	enum Actions { Idle = 0, MoveLeft = 1, MoveRight = 2, ThumpingBroom = 3, Sweeping = 4, WalkingBroomLeft = 5, WalkingBroomRight = 6 }
	Actions[] baseActions = {Actions.Idle, Actions.MoveLeft, Actions.MoveRight};
	Actions[] actionsInsideAngry = {Actions.Idle, Actions.MoveLeft, Actions.MoveRight, Actions.ThumpingBroom};
	Actions[] actionsOutside = {Actions.Idle, Actions.MoveLeft, Actions.MoveRight};

	const int DEFAULT_CHARACTER_LAYER = 9;
	const int IN_FRONT_OF_BUILDING_CHARACTER_LAYER = 12;
	const int DEFAULT_ROOM_LAYER = 8;

	Animator animator;
	SpriteRenderer spriteRenderer, roomLayer;
	TimeController timeController;

	private IEnumerator roomTransition;

	public float leftRoomBoundary;
	public float rightRoomBoundary;
	public float leftOutsideBoundary;
	public float rightOutsideBoundary;
	public float speed;
	public int start = 0;
	public int goesOutsideFromRoom = 45;
	// public int goesToLeftFlowersFromOutside = 60;
	public int goesToRoomFromOutside = 120;
	// public int angryInRoom = 140;
	// public int bangsBroom1 = 160;
	// public int stopBroom1 = 170;
	// public int bangsBroom2 = 180;
	// public int stopBroom2 = 190;
	// public int bangsBroom3 = 200;
	// public int stopBroom3 = 210;
	// public int bangsBroom4 = 220;
	// public int stopBroom4 = 230; 
	public int cleansUpUrn = 240;
	// public int contemplatesMurder = 260;
	public int dissapearsToMurder = 280;
	// private bool canAnimate = true;
	//initialize to standing
	private int currentAction;
	private int randomDecisionTime;
	private int nextActionTime;

	const string LOCATION_INSIDE = "insideRoom";
	const string LOCATION_OUTSIDE = "outside";
	const string LOCATION_INSIDE_ANGRY = "insideRoomAngry";

	private bool goOutsideFromInsideCoroutineOnce = true;
	private bool goInsideFromOutsideCoroutineOnce = true;
	private bool leaveToMurderCoroutineOnce = true;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		roomLayer = GameObject.Find ("Ms Tabby Room").GetComponent<SpriteRenderer> ();
		timeController = GameObject.Find ("Clock").GetComponent<TimeController> ();

		spriteRenderer.sortingOrder = DEFAULT_CHARACTER_LAYER;
		roomLayer.sortingOrder = DEFAULT_ROOM_LAYER;

		ChooseNextAction (baseActions, 2.0f, 4.0f);
	}

	// Update is called once per frame
	void Update () {
		if (CheckIfInTime (start, goesOutsideFromRoom)) {
			DetermineActionFromLocation (LOCATION_INSIDE);
			CheckIfOutOfBounds(leftRoomBoundary, rightRoomBoundary);
		}
		if (CheckIfInTime (goesOutsideFromRoom, 0)) {
			if (goOutsideFromInsideCoroutineOnce) {
				spriteRenderer.sortingOrder = DEFAULT_CHARACTER_LAYER - 2;
				roomLayer.sortingOrder = DEFAULT_ROOM_LAYER - 2;
				SetXScale (-.5f);
				animator.SetInteger ("State", 1);
				StartCoroutine ("GoOutsideFromInside");
				goOutsideFromInsideCoroutineOnce = false;
			}
		}
		if (CheckIfInTime (goesOutsideFromRoom + 15, goesToRoomFromOutside)) {
			DetermineActionFromLocation (LOCATION_OUTSIDE);
			CheckIfOutOfBounds(leftOutsideBoundary, rightOutsideBoundary);
		}
		if (CheckIfInTime (goesToRoomFromOutside, 0)) {
			if (goInsideFromOutsideCoroutineOnce) {
				animator.SetInteger ("State", 1);
				StartCoroutine ("GoInsideFromOutside");
				goInsideFromOutsideCoroutineOnce = false;
			}
		}
		if (CheckIfInTime(goesToRoomFromOutside + 20, cleansUpUrn)) {
			DetermineActionFromLocation (LOCATION_INSIDE_ANGRY);
			CheckIfOutOfBounds(leftRoomBoundary, rightRoomBoundary);
		}
		if (CheckIfInTime(cleansUpUrn, dissapearsToMurder)) {
			animator.SetInteger ("State", 4);
		}
		if (CheckIfInTime (dissapearsToMurder, 0)) {
			if (leaveToMurderCoroutineOnce) {
				spriteRenderer.sortingOrder = DEFAULT_CHARACTER_LAYER - 2;
				roomLayer.sortingOrder = DEFAULT_ROOM_LAYER - 2;
				animator.SetInteger ("State", 3);
				roomTransition = RoomTransition(transform.position, new Vector3 (-7f, transform.position.y, transform.position.z));
				StartCoroutine (roomTransition);
				leaveToMurderCoroutineOnce = false;
			}
		}
	}

	void DetermineActionFromLocation (string location) {
		if (timeController.timeInSeconds == nextActionTime) {
			if(location.Equals(LOCATION_INSIDE)) {
				ChooseNextAction (baseActions, 2.0f, 4.0f);
			} else if(location.Equals(LOCATION_OUTSIDE)) {
				ChooseNextAction (actionsOutside, 2.0f, 10.0f);
			} else if(location.Equals(LOCATION_INSIDE_ANGRY)) {
				ChooseNextAction (actionsInsideAngry, 3.0f, 5.0f);
			}
	 	}
		SetAction();
	}

	void ChooseNextAction (Actions[] actionList, float minDecisionTime, float maxDecisionTime) {
		int nextAction = Mathf.RoundToInt (Random.Range (0.0f, actionList.Length - 1));
		currentAction = (int)actionList[nextAction];

		// Debug.Log("Current Action for " + this.gameObject.name  + ": " + currentAction);

		randomDecisionTime = Mathf.RoundToInt (Random.Range (minDecisionTime, maxDecisionTime));
		nextActionTime = timeController.timeInSeconds + randomDecisionTime;
	}

	void SetAction() {
		if (currentAction == (int) Actions.Idle) {
			animator.SetInteger ("State", 0);
			transform.position = transform.position;
		}
		if (currentAction == (int) Actions.MoveLeft) {
			animator.SetInteger ("State", 1);
			transform.position += Vector3.left * Time.deltaTime * speed;
			// transform.localScale = new Vector3 (-.5f, transform.localScale.y, transform.localScale.z);
			SetXScale (-.5f);
		}
		if (currentAction == (int) Actions.MoveRight) {
			animator.SetInteger ("State", 1);
			transform.position += Vector3.right * Time.deltaTime * speed;
			// transform.localScale = new Vector3 (.5f, transform.localScale.y, transform.localScale.z);
			SetXScale (.5f);
		}
		if (currentAction == (int) Actions.ThumpingBroom) {
			animator.SetInteger ("State", 2);
		}
	}

	void CheckIfOutOfBounds(float leftBoundary, float rightBoundary) {
		if (transform.position.x < leftBoundary) {
			currentAction = (int) Actions.MoveRight;
		}
		if (transform.position.x > rightBoundary) {
			currentAction = (int) Actions.MoveLeft;
		}
	}

	bool CheckIfInTime (int beginningTime, int endTime) {
		if (endTime == 0 && timeController.timeInSeconds == beginningTime) {
			return true;
		}

		if (beginningTime < timeController.timeInSeconds && timeController.timeInSeconds < endTime)
			return true;
		else
			return false;
	}

	void SetXScale (float scale) {
		transform.localScale = new Vector3 (scale, transform.localScale.y, transform.localScale.z);
	}

	private IEnumerator RoomTransition (Vector3 startPos, Vector3 endPos) {
		float elapsedTime = 0f;
		float secondsToTransitionToRoom = 5.0f;

		if(startPos.x < endPos.x) {
			SetXScale(.5f);
		} else {
			SetXScale(-.5f);
		}

		while (elapsedTime < secondsToTransitionToRoom) {
			transform.position = Vector3.Lerp (startPos, endPos, elapsedTime / secondsToTransitionToRoom);
			elapsedTime += Time.deltaTime;
			yield return null;
		}

		animator.SetInteger ("State", 0);
		nextActionTime = timeController.timeInSeconds + 1;
		StopCoroutine ("RoomTransition");
	}

	private IEnumerator GoOutsideFromInside () {
		// Good Lord
		// We are lerping to a specific spot over a set amount of time.
		// We use elapsedTime to know how much time has passed from time.deltaTime since the coroutine has started
		// The seconds variable are how long it takes to get to the spot FROM 0 real shoddy stuff
		float elapsedTime = 0f;
		float secondsToGoOffRoomView = 5.0f;
		float secondsToGoToOutside = 15.0f;

		// We set the start position here, if we pass in transform.position to the lerp the character moves really fast
		Vector3 startPos = transform.position;

		while (elapsedTime < secondsToGoOffRoomView) {
			transform.position = Vector3.Lerp (startPos, new Vector3 (-7f, transform.position.y, transform.position.z), elapsedTime / secondsToGoOffRoomView);
			elapsedTime += Time.deltaTime;
			yield return null;
		}
		// Debug.Log ("elapsedTime: " + elapsedTime);
		// New Start position based off of where the character ends
		startPos = new Vector3 (-7f, transform.position.y, transform.position.z);

		while (secondsToGoOffRoomView < elapsedTime && elapsedTime < secondsToGoToOutside) {
			// So we have to take the seconds - the last event time to get something close to 0
			// Like a said, real shoddy stuff
			transform.position = Vector3.Lerp (startPos, new Vector3 (0f, -17.37f, transform.position.z), (elapsedTime - secondsToGoOffRoomView) / (secondsToGoToOutside- secondsToGoOffRoomView));
			elapsedTime += Time.deltaTime;
			yield return null;
		}
		// Debug.Log ("elapsedTime: " + elapsedTime);
		// Debug.Log ("coroutine is stopping");
		// Set the sorting order of the character and room to what they should be
		spriteRenderer.sortingOrder = IN_FRONT_OF_BUILDING_CHARACTER_LAYER;
		roomLayer.sortingOrder = DEFAULT_ROOM_LAYER;
		animator.SetInteger ("State", 0);
		nextActionTime = timeController.timeInSeconds + 1;
		StopCoroutine ("GoOutsideFromInside");
	}

	private IEnumerator GoInsideFromOutside () {
		float elapsedTime = 0f;
		float secondsToGoToFrontDoor = 5.0f;
		float secondsToGoInside = 15.0f;
		float secondsToGoInRoom = 20.0f;
		if(transform.position.x < 0) {
			SetXScale (.5f);
		} else {
			SetXScale (-.5f);
		}
		Vector3 startPos = transform.position;
		animator.SetInteger ("State", 1);
		while (elapsedTime < secondsToGoToFrontDoor) {
			transform.position = Vector3.Lerp (startPos, new Vector3 (0f, transform.position.y, transform.position.z), elapsedTime / secondsToGoToFrontDoor);
			elapsedTime += Time.deltaTime;
			yield return null;
		}
		spriteRenderer.sortingOrder = DEFAULT_CHARACTER_LAYER - 4;
		roomLayer.sortingOrder = DEFAULT_ROOM_LAYER - 4;
		// Debug.Log ("elapsedTime: " + elapsedTime);
		startPos = transform.position;
		while (secondsToGoToFrontDoor < elapsedTime && elapsedTime < secondsToGoInside) {
			transform.position = Vector3.Lerp (startPos, new Vector3 (-7f, 12.8f, transform.position.z), (elapsedTime - secondsToGoToFrontDoor) / (secondsToGoInside - secondsToGoToFrontDoor));
			elapsedTime += Time.deltaTime;
			yield return null;
		}
		startPos = new Vector3 (-7f, 12.8f, transform.position.z);
		SetXScale (.5f);
		while (secondsToGoInside < elapsedTime && elapsedTime < secondsToGoInRoom) {
			transform.position = Vector3.Lerp (startPos, new Vector3 (0, transform.position.y, transform.position.z), (elapsedTime - secondsToGoInside) / (secondsToGoInRoom - secondsToGoInside));
			elapsedTime += Time.deltaTime;
			yield return null;
		}
		spriteRenderer.sortingOrder = DEFAULT_CHARACTER_LAYER;
		roomLayer.sortingOrder = DEFAULT_ROOM_LAYER;
		// Debug.Log ("elapsedTime: " + elapsedTime);
		// Debug.Log ("coroutine is stopping");
		animator.SetInteger ("State", 0);
		nextActionTime = timeController.timeInSeconds + 1;
		StopCoroutine ("GoInsideFromOutside");
	}
}