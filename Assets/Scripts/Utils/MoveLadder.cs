﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLadder : MonoBehaviour {

	public float speed;

	Vector3[] point = {
		new Vector3 (16.409f, -15.647f, 0f),
		new Vector3 (16.409f, -10.13f, 0f)
	};

	TimeController timeController;

	private IEnumerator moveLadderCoroutine;

	public float toValue;
	public float fromValue;

	private bool startCoroutine = true;

	// Use this for initialization
	void Start () {
		timeController = GameObject.Find ("Clock").GetComponent<TimeController> ();
	}

	// Update is called once per frame
	void Update () {
		if (timeController.timeInSeconds == 135 && startCoroutine) {
			moveLadderCoroutine = MoveLadderCoroutine(2f);
			StartCoroutine(moveLadderCoroutine);
			startCoroutine = false;
		}
	}

	private IEnumerator MoveLadderCoroutine (float time) {
		float elapsedTime = 0f;

		while (elapsedTime < time) {
			transform.position = Vector3.Lerp (point[0], point[1], elapsedTime / time);
			elapsedTime += Time.deltaTime;
			yield return null;
		}

		StopCoroutine ("MoveLadderCoroutine");
	}
}
