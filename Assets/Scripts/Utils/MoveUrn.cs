﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUrn : MonoBehaviour {
	private float curveMultiplier = 1f;

	Vector3[] point = {
		new Vector3 (3.96f, 13.56f, 0f),
		new Vector3 (),
		new Vector3 (3.43f, 13.56f, 0f)
	};
	Vector3[] point1 = {
		new Vector3 (3.43f, 13.56f, 0f),
		new Vector3 (),
		new Vector3 (2.81f, 13.56f, 0f)
	};
	Vector3[] point2 = {
		new Vector3 (2.81f, 13.56f, 0f),
		new Vector3 (),
		new Vector3 (1.8f, 13.56f, 0f)
	};
	Vector3[] point3 = {
		new Vector3 (1.8f, 13.56f, 0f),
		new Vector3 (),
		new Vector3 (.89f, 10.04f, 0f)
	};

	TimeController timeController;

	private IEnumerator moveUrnCoroutine;

	public float startTime;

	private bool startCoroutine = true;

	// Use this for initialization
	void Start () {
		timeController = GameObject.Find ("Clock").GetComponent<TimeController> ();
	}

	// Update is called once per frame
	void Update () {
		if (timeController.timeInSeconds == startTime && startCoroutine) {
			moveUrnCoroutine = CalculateUrnArc(1f, point, curveMultiplier);
			StartCoroutine(moveUrnCoroutine);
			startCoroutine = false;
		}
		if (timeController.timeInSeconds == startTime + 1) {
			startCoroutine = true;
		}
		if (timeController.timeInSeconds == startTime + 2 && startCoroutine) {
			moveUrnCoroutine = CalculateUrnArc(1f, point1, curveMultiplier);
			StartCoroutine(moveUrnCoroutine);
			startCoroutine = false;
		}
		if (timeController.timeInSeconds == startTime + 3) {
			startCoroutine = true;
		}
		if (timeController.timeInSeconds == startTime + 4 && startCoroutine) {
			moveUrnCoroutine = CalculateUrnArc(1f, point2, curveMultiplier);
			StartCoroutine(moveUrnCoroutine);
			startCoroutine = false;
		}
		if (timeController.timeInSeconds == startTime + 5) {
			startCoroutine = true;
		}
		if (timeController.timeInSeconds == startTime + 6 && startCoroutine) {
			moveUrnCoroutine = CalculateUrnArc(1f, point3, 5f);
			StartCoroutine(moveUrnCoroutine);
			startCoroutine = false;
		}
		if (timeController.timeInSeconds == startTime + 7) {
			startCoroutine = true;
		}
	}

	private IEnumerator CalculateUrnArc (float time, Vector3[] curves, float curveArc) {
		float elapsedTime = 0f;

		curves[1] = curves[0] + (curves[2] - curves[0]) / 2 + Vector3.up * curveArc;

		while (elapsedTime < time) {
			Vector3 m1 = Vector3.Lerp (curves[0], curves[1], elapsedTime / time);
			Vector3 m2 = Vector3.Lerp (curves[1], curves[2], elapsedTime / time);
			transform.position = Vector3.Lerp (m1, m2, elapsedTime / time);
			elapsedTime += Time.deltaTime;
			yield return null;
		}

		StopCoroutine ("CalculateUrnArc");
	}
}
