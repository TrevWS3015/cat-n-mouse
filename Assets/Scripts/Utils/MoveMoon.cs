﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveMoon : MonoBehaviour {

	public float speed;

	Vector3[] point = {
		new Vector3 (),
		new Vector3 (),
		new Vector3 (-24.2f, 18.5f, 0f)
	};

	TimeController timeController;

	private IEnumerator moveMoonCoroutine;

	public float toValue;
	public float fromValue;

	private bool startCoroutine = true;

	// Use this for initialization
	void Start () {
		point[0] = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
		point[1] = point[0] + (point[2] - point[0]) / 2 + Vector3.up * 5.0f;
		timeController = GameObject.Find ("Clock").GetComponent<TimeController> ();
	}

	// Update is called once per frame
	void Update () {
		if (timeController.timeInSeconds == 160 && startCoroutine) {
			moveMoonCoroutine = CalculateArc(10f);
			StartCoroutine(moveMoonCoroutine);
			startCoroutine = false;
		}
	}

	private IEnumerator CalculateArc (float time) {
		float elapsedTime = 0f;

		while (elapsedTime < time) {
			Vector3 m1 = Vector3.Lerp (point[0], point[1], elapsedTime / time);
			Vector3 m2 = Vector3.Lerp (point[1], point[2], elapsedTime / time);
			transform.position = Vector3.Lerp (m1, m2, elapsedTime / time);
			elapsedTime += Time.deltaTime;
			yield return null;
		}

		StopCoroutine ("CalculateArc");
	}
}