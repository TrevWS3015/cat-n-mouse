﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VanTurco : MonoBehaviour {

	enum Actions { Idle = 0, MoveLeft = 1, MoveRight = 2 }

	Actions[] baseActions = {Actions.Idle, Actions.MoveLeft, Actions.MoveRight};
	
	const int DEFAULT_CHARACTER_LAYER = 9;
	const int IN_FRONT_OF_BUILDING_CHARACTER_LAYER = 12;
	const int DEFAULT_ROOM_LAYER = 8;

	static Vector3 outsideRoomBalcony = new Vector3 (18.75f, 4.52f, 0f);
	static Vector3 insideRoom = new Vector3 (10f, 4.52f, 0f);
	static Vector3 outsideRoom = new Vector3 (3f, 4.52f, 0f);
	static Vector3 behindDoor = new Vector3 (0f, -17.3f, 0f);
	static Vector3 inFrontOfBike = new Vector3 (18.75f, -17.24f, 0f);
	static Vector3 outOfView = new Vector3 (39f, -17.24f, 0f);

	Transitions[] toOffScreenFromRoom = {
		new Transitions (outsideRoomBalcony, 5f, DEFAULT_CHARACTER_LAYER - 4, DEFAULT_ROOM_LAYER - 4, 1),
		new Transitions (inFrontOfBike, 8f, DEFAULT_CHARACTER_LAYER - 4, DEFAULT_ROOM_LAYER - 4, 3),
		new Transitions (outOfView, 2f, DEFAULT_CHARACTER_LAYER, DEFAULT_ROOM_LAYER, 2)
	};
	Transitions[] toRoomFromOffScreen = {
		new Transitions (inFrontOfBike, 2f, DEFAULT_CHARACTER_LAYER - 4, DEFAULT_ROOM_LAYER - 4, 2),
		new Transitions (outsideRoomBalcony, 8f, DEFAULT_CHARACTER_LAYER - 4, DEFAULT_ROOM_LAYER - 4, 3),
		new Transitions (outsideRoom, 5f, DEFAULT_CHARACTER_LAYER - 3, DEFAULT_ROOM_LAYER - 3, 1)
	};
	Transitions[] toAngerSpot = {
		new Transitions (inFrontOfBike, 2f, DEFAULT_CHARACTER_LAYER - 4, DEFAULT_ROOM_LAYER, 2)
	};
	Transitions[] toRoomFromRoad = {
		new Transitions (behindDoor, 5f, IN_FRONT_OF_BUILDING_CHARACTER_LAYER - 4, DEFAULT_ROOM_LAYER - 4, 4),
		new Transitions (outsideRoom, 5f, DEFAULT_CHARACTER_LAYER - 3, DEFAULT_ROOM_LAYER, 4),
		new Transitions (insideRoom, 5f, DEFAULT_CHARACTER_LAYER - 3, DEFAULT_ROOM_LAYER - 3, 4)
	};
	Transitions[] toOffScreenFromRoomFinal = {
		new Transitions (outsideRoom, 2f, DEFAULT_CHARACTER_LAYER - 4, DEFAULT_ROOM_LAYER - 4, 4),
		new Transitions (behindDoor, 3f, DEFAULT_CHARACTER_LAYER - 4, DEFAULT_ROOM_LAYER - 4, 4),
		new Transitions (inFrontOfBike, 3f, IN_FRONT_OF_BUILDING_CHARACTER_LAYER, DEFAULT_ROOM_LAYER, 4),
		new Transitions (outOfView, 2f, DEFAULT_CHARACTER_LAYER, DEFAULT_ROOM_LAYER, 2)
	};

	Animator animator;
	SpriteRenderer spriteRenderer, roomLayer;
	TimeController timeController;

	public GameObject motorcycle, motorcycleClone;
	private Vector3 motorcyclePosition;

	private IEnumerator roomTransition;

	private int currentAction;
	private int randomDecisionTime;
	private int nextActionTime;
	public float leftRoomBoundary;
	public float rightRoomBoundary;
	public float speed;
	public int leavesRoom1 = 0;
	public int destroyMotorcycle1 = 13;
	public int goesToRoom1 = 45;
	public int createMotorcycle1 = 47;
	public int leavesRoom2 = 60;
	public int destroyMotorcycle2 = 73;
	public int goesToRoom2 = 95;
	public int createMotorcycle2 = 97;
	public int leavesRoom3 = 115;
	public int destroyMotorcycle3 = 128;
	public int comesBack3rdTime = 160;
	public int createMotorcycle3 = 162;
	public int getsPissed = 165;
	public int goesInsideFromLobby = 185;
	public int looksPissedInRoom = 200;
	public int leavesQuickly = 290;
	public int destroyMotorcycle4 = 298;

	const string LOCATION_INSIDE = "insideRoom";

	private bool canStartCoroutine = true;
	private bool canCreateMotorcycle = false;

	private bool goOutsideToSingCoRoutine = true;
	private bool goInsideToSingCoRoutine = true;
	
	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		timeController = GameObject.Find ("Clock").GetComponent<TimeController> ();
		roomLayer = GameObject.Find ("VanTurcos Room").GetComponent<SpriteRenderer> ();

		spriteRenderer.sortingOrder = DEFAULT_CHARACTER_LAYER;
		roomLayer.sortingOrder = DEFAULT_ROOM_LAYER;

		motorcyclePosition = new Vector3(motorcycle.transform.position.x, motorcycle.transform.position.y, 0f);
		motorcycleClone = Instantiate(motorcycle, motorcycle.transform.position, Quaternion.identity);

		ChooseNextAction (baseActions, 2.0f, 4.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if(CheckIfInTime(leavesRoom1, 0)) {
			if (canStartCoroutine) {
				roomTransition = RoomTransition (toOffScreenFromRoom);
				StartCoroutine (roomTransition);
				canStartCoroutine = false;
			}
		}
		if(CheckIfInTime(destroyMotorcycle1, 0)) {
			if(motorcycle != null) {
				Destroy(motorcycleClone, .5f);
				canStartCoroutine = true;
				canCreateMotorcycle = true;
			}
		}
		if(CheckIfInTime(goesToRoom1, 0)) {
			if (canStartCoroutine) {
				roomTransition = RoomTransition (toRoomFromOffScreen);
				StartCoroutine (roomTransition);
				canStartCoroutine = false;
			}
		}
		if(CheckIfInTime(createMotorcycle1, 0)) {
			if(canCreateMotorcycle) {
				motorcycleClone = Instantiate(motorcycle, motorcycle.transform.position, Quaternion.identity);
				canStartCoroutine = true;
				canCreateMotorcycle = false;
			}
			SetXScale(.5f);
		}

		if(CheckIfInTime(leavesRoom2, 0)) {
			if (canStartCoroutine) {
				roomTransition = RoomTransition (toOffScreenFromRoom);
				StartCoroutine (roomTransition);
				canStartCoroutine = false;
			}
		}
		if(CheckIfInTime(destroyMotorcycle2, 0)) {
			if(motorcycle != null) {
				Destroy(motorcycleClone, 0f);
				canStartCoroutine = true;
				canCreateMotorcycle = true;
			}
		}
		if(CheckIfInTime(goesToRoom2, 0)) {
			if (canStartCoroutine) {
				roomTransition = RoomTransition (toRoomFromOffScreen);
				StartCoroutine (roomTransition);
				canStartCoroutine = false;
			}
		}
		if(CheckIfInTime(createMotorcycle2, 0)) {
			if(canCreateMotorcycle) {
				motorcycleClone = Instantiate(motorcycle, motorcycle.transform.position, Quaternion.identity);
				canStartCoroutine = true;
				canCreateMotorcycle = false;
			}
			SetXScale(.5f);
		}

		if(CheckIfInTime(leavesRoom3, 0)) {
			if (canStartCoroutine) {
				roomTransition = RoomTransition (toOffScreenFromRoom);
				StartCoroutine (roomTransition);
				canStartCoroutine = false;
			}
		}
		if(CheckIfInTime(destroyMotorcycle3, 0)) {
			if(motorcycle != null) {
				Destroy(motorcycleClone, 0f);
				canStartCoroutine = true;
				canCreateMotorcycle = true;
			}
		}
		if(CheckIfInTime(comesBack3rdTime, 0)) {
			if (canStartCoroutine) {
				roomTransition = RoomTransition (toAngerSpot);
				StartCoroutine (roomTransition);
				canStartCoroutine = false;
			}
		}
		if(CheckIfInTime(createMotorcycle3, 0)) {
			if(canCreateMotorcycle) {
				motorcycleClone = Instantiate(motorcycle, motorcycle.transform.position, Quaternion.identity);
				canStartCoroutine = true;
				canCreateMotorcycle = false;
			}
			SetXScale(.5f);
			animator.SetInteger ("State", 5);
		}
		if(CheckIfInTime(createMotorcycle3, goesInsideFromLobby)) {
			animator.SetInteger ("State", 5);
		}
		if(CheckIfInTime(goesInsideFromLobby, 0)) {
			if (canStartCoroutine) {
				roomTransition = RoomTransition (toRoomFromRoad);
				StartCoroutine (roomTransition);
				canStartCoroutine = false;
			}
		}
		if(CheckIfInTime(looksPissedInRoom, leavesQuickly)) {
			DetermineActionFromLocation (LOCATION_INSIDE);
			CheckIfOutOfBounds (leftRoomBoundary, rightRoomBoundary);
			canStartCoroutine = true;
		}
		if(CheckIfInTime(leavesQuickly, 0)) {
			if (canStartCoroutine) {
				roomTransition = RoomTransition (toOffScreenFromRoomFinal);
				StartCoroutine (roomTransition);
				canStartCoroutine = false;
			}
		}
		if(CheckIfInTime(destroyMotorcycle4, 0)) {
			if(motorcycle != null) {
				Destroy(motorcycleClone, 0f);
				canStartCoroutine = true;
				canCreateMotorcycle = true;
			}
		}
	}

	void DetermineActionFromLocation (string location) {
		if (timeController.timeInSeconds == nextActionTime) {
			if (location.Equals (LOCATION_INSIDE)) {
				ChooseNextAction (baseActions, 2.0f, 4.0f);
			} 
		}
		SetAction ();
	}

	void ChooseNextAction (Actions[] actionList, float minDecisionTime, float maxDecisionTime) {
		int nextAction = Mathf.RoundToInt (Random.Range (0.0f, actionList.Length - 1));
		currentAction = (int) actionList[nextAction];

		// Debug.Log ("Current Action for " + this.gameObject.name + ": " + currentAction);

		randomDecisionTime = Mathf.RoundToInt (Random.Range (minDecisionTime, maxDecisionTime));
		nextActionTime = timeController.timeInSeconds + randomDecisionTime;
	}

	void SetAction () {
		if (currentAction == (int) Actions.Idle) {
			animator.SetInteger ("State", 5);
			transform.position = transform.position;
		}
		if (currentAction == (int) Actions.MoveLeft) {
			animator.SetInteger ("State", 4);
			transform.position += Vector3.left * Time.deltaTime * speed;
			// transform.localScale = new Vector3 (-.5f, transform.localScale.y, transform.localScale.z);
			SetXScale (-.5f);
		}
		if (currentAction == (int) Actions.MoveRight) {
			animator.SetInteger ("State", 4);
			transform.position += Vector3.right * Time.deltaTime * speed;
			// transform.localScale = new Vector3 (.5f, transform.localScale.y, transform.localScale.z);
			SetXScale (.5f);
		}
	}

	void CheckIfOutOfBounds (float leftBoundary, float rightBoundary) {
		if (transform.position.x < leftBoundary) {
			currentAction = (int) Actions.MoveRight;
		}
		if (transform.position.x > rightBoundary) {
			currentAction = (int) Actions.MoveLeft;
		}
	}

	bool CheckIfInTime (int beginningTime, int endTime) {
		if (endTime == 0 && timeController.timeInSeconds == beginningTime) {
			return true;
		}

		if (beginningTime < timeController.timeInSeconds && timeController.timeInSeconds < endTime)
			return true;
		else
			return false;
	}

	void SetXScale (float scale) {
		transform.localScale = new Vector3 (scale, transform.localScale.y, transform.localScale.z);
	}

	void setLayers (int newCharLayer, int newRoomLayer) {
		spriteRenderer.sortingOrder = newCharLayer;
		roomLayer.sortingOrder = newRoomLayer;
	}

	private IEnumerator RoomTransition (Transitions[] positionsToMoveTo) {
		float elapsedTime;
		Vector3 startPos;

		for (int i = 0; i < positionsToMoveTo.Length; i++) {
			elapsedTime = 0f;
			startPos = transform.position;

			if (transform.position.x < positionsToMoveTo[i].position.x) {
				SetXScale (.5f);
			} else {
				SetXScale (-.5f);
			}

			setLayers (positionsToMoveTo[i].characterLayer, positionsToMoveTo[i].roomLayer);
			animator.SetInteger ("State", positionsToMoveTo[i].characterAnimationState);

			while (elapsedTime < positionsToMoveTo[i].transitionTime) {
				transform.position = Vector3.Lerp (startPos, positionsToMoveTo[i].position, elapsedTime / positionsToMoveTo[i].transitionTime);
				elapsedTime += Time.deltaTime;
				yield return null;
			}
		}

		nextActionTime = timeController.timeInSeconds + 1;
		StopCoroutine ("RoomTransition");
	}
}