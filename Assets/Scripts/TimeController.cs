﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour {
	public int timeInSeconds = 0;
	public float frameTime = 0.0f;
	public float timeUp = 300.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		frameTime += Time.deltaTime;
		timeInSeconds = (Mathf.RoundToInt(frameTime));
		// Debug.Log(timeInSeconds);
	}
}
